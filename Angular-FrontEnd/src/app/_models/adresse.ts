export class Adresse {
    idAdresse: number;
    numero: number;
    lieuDit: string;
    nomLieu: string;
    complementAd: string;
    codePostal:number;
    ville: string;
    pays: string;
    idClient:number;
}
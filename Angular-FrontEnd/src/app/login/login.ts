﻿export interface ILogin {
    id_client: number,
    mail: String,
    nom: String,
    prenom: String,
    password: String,
    tel_perso: String
}

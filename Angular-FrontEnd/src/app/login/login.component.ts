﻿// Le composant de connexion utilise le service d'authentification pour se connecter à l'application. Si l'utilisateur est déjà connecté, il est automatiquement redirigé vers la page d'accueil.
// L'objet loginForm: FormGroup définit les contrôles et les validateurs du formulaire et est utilisé pour accéder aux données entrées dans le formulaire.
// FormGroup fait partie du module Angular Reactive Forms et est lié au modèle de connexion avec la directive [formGroup]="loginForm"

import { Component, OnInit, ViewChild, ElementRef, QueryList, ViewChildren,  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, AuthenticationService } from '../_services';
import { LoginService } from './login.service';
import { ILogin } from './login';
//@Component({ templateUrl: 'login.component.html' })

@Component({
    template: `
    <h2>Connectez vous </h2>
    <form [formGroup]="loginForm" (ngSubmit)="onSubmit()">
        <div class="form-group">
        <label for="mail">Adresse e-mail</label>
        <input type="text" formControlName="mail" class="form-control" [ngClass]="{ 'is-invalid': submitted && f.mail.errors }" />
        <div *ngIf="submitted && f.mail.errors" class="invalid-feedback">
            <div *ngIf="f.mail.errors.required">mail is required</div>
        </div>
    </div>
    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" formControlName="password" class="form-control" [ngClass]="{ 'is-invalid': submitted && f.password.errors }" />
        <div *ngIf="submitted && f.password.errors" class="invalid-feedback">
            <div *ngIf="f.password.errors.required">Password is required</div>
        </div>
    </div>
    <div class="form-group">
        <button  [disabled]="loading" class="btn btn-primary">
            <span *ngIf="loading" class="spinner-border spinner-border-sm mr-1"></span>
            Se connecter
        </button>
        <a routerLink="/register" class="btn btn-link">Créer votre compte Admin</a>
    </div>
    </form>
    `,
    styles: []
  })

export class LoginComponent implements OnInit {

    public logins;

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    errorMsg: string;


    constructor(
        public _loginService: LoginService,
        public formBuilder: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
        public authenticationService: AuthenticationService,
        public alertService: AlertService,
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {

        this.loginForm = this.formBuilder.group({
            mail: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        console.log(this.f.mail.value)
        console.log(this.f.password.value)

        this.loading = true;
        this.authenticationService.login(this.f.mail.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}

import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { HttpHeaders } from '@angular/common/http';
import { ILogin } from './login';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

  public _Url: string;
  public posts: Observable<any>

  constructor(private http: HttpClient) {
    this._Url = 'http://localhost:8080/admin/check';
  }

  public checkUser(mail:string, password:string){

    const httpHeaders: HttpHeaders = new HttpHeaders({'mail': mail, 'password': password})
    httpHeaders.append('Content-Type','application/json');
    httpHeaders.append('Accept','/');

    return this.http.get(this._Url, { headers: httpHeaders })
    .pipe(tap(data => alert(JSON.stringify(data))) , catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return observableThrowError(error.message || "Server Error");
  }





}

export class Annonce {
       idAnnonce? : number
       adresse? : string
       surface? : number
       nbrePiece? : number
       equipement? : string
       description? : string
       etoile? : number
       dateDebut? : Date
       dateFin? : Date
}

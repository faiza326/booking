﻿//Faux fournisseur d'API backend
// Le faux fournisseur d’API backend permet à l’exemple de s’exécuter sans backend / backendless
// il utilise le stockage local HTML5 pour stocker les données d’utilisateur enregistrées et fournit de fausses implémentations pour les méthodes d’authentification et CRUD
// Celles-ci seraient gérées par une véritable API et une base de données dans une application de production.

// Il est implémenté à l'aide de la classe HttpInterceptor introduite dans Angular 4.3 avec le nouveau HttpClientModule
// En étendant la classe HttpInterceptor, vous pouvez créer un intercepteur personnalisé pour modifier les demandes http avant qu'elles ne soient envoyées au serveur.
// Dans ce cas, FakeBackendInterceptor intercepte certaines demandes en fonction de leur URL et fournit une fausse réponse au lieu d'aller au serveur.


import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { RegisterService } from '../register/register.service';
import { LoginService } from '../login/login.service';

// array in local storage for registered users
let users = JSON.parse(localStorage.getItem('users')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    //public _registerservice : RegisterService

    constructor(
        public _registerservice: RegisterService,
        public _loginService: LoginService,
    ) {}


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }
        }

        // route functions

        function authenticate() {
            const { mail, password } = body;

            const user = users.find(x => x.mail === mail && x.password === password);

            console.log(user)

            //if (!user) return error('Username or password is incorrect');

            sessionStorage.setItem('id', "ID");

            //initialiser la variable locale listCommande
            localStorage['listCommande'] = ''

            return ok({
                id_client: user.id_client,
                mail: user.mail,
                nom: user.nom,
                prenom: user.prenom,
                password: user.password,
                token: 'fake-jwt-token'
            })
        }

        function register() {

            return ok();
        }

        function ajouter_produit() {
            return ok();
        }

        function getUsers() {

            localStorage.getItem('currentUser');
            if (localStorage.getItem('currentUser') == '[]') return unauthorized();


            return ok(users);
        }

        function deleteUser() {
            if (!isLoggedIn()) return unauthorized();

            users = users.filter(x => x.idClient !== idFromUrl());
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message){
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};

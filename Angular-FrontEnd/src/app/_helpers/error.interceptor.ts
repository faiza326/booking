// Intercepteur d'erreur HTTP
// L'intercepteur d'erreur intercepte les rÃ©ponses http de l'API pour vÃ©rifier s'il y a eu des erreurs.
// S'il y a une rÃ©ponse 401 non autorisÃ©e, l'utilisateur est automatiquement dÃ©connectÃ© de l'application
// Toutes les autres erreurs sont renvoyÃ©es au service appelant afin qu'une alerte puisse Ãªtre affichÃ©e Ã  l'utilisateur.


import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload(true);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}

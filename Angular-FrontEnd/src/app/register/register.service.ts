import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

  public _Url: string;
  public localVar;


  constructor(private http: HttpClient) {
    this._Url = 'http://localhost:8080/admin/ajouter';
  }

  public addUser(user : any){

    const httpHeaders: HttpHeaders = new HttpHeaders({'mail': user[0], 'nom': user[1], 'prenom':user[2], 'password':user[3]})
    httpHeaders.append('Content-Type','application/json');
    httpHeaders.append('Accept','/');


    var data = {
        'mail': 'mail', //user[0].toString(),
        'nom': "", //user[1].toString(),
        'prenom': "jean", //user[2].toString(),
        'password': "jean", //user[3].toStrin)g(),

    }

    return this.http.post('http://localhost:8080/admin/ajouter', data,  { headers: httpHeaders })
    .subscribe(
        res =>{
            console.log(res);
        },
        err => {
            console.log(err.message);
        }
    )}
}

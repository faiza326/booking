﻿// Le composant d'enregistrement cree un nouvel utilisateur avec le service utilisateur lors de l'envoi du formulaire de registre.
// Si l'utilisateur est deja  connectee, il est automatiquement redirigÃ© vers la page d'accueil.
// L'objet registerForm: FormGroup dÃ©finit les contrÃ´les de formulaire et les validateurs.
// Il est utilise pour acceder aux donnÃ©es entrees dans le formulaire.
// FormGroup fait partie du module Angular Reactive Forms et est liÃ© au modÃ¨le de connexion ci-dessus avec la directive [formGroup]="registerForm"

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, UserService, AuthenticationService } from '../_services';
import { RegisterService } from './register.service';


@Component({ templateUrl: 'register.component.html' })
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        public _registerservice: RegisterService,
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            mail: ['', Validators.required],
            nom: ['', Validators.required],
            prenom: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],

        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        //Recuperation des données formulaire de l'utilisateur
        var user = [
            this.registerForm.get('mail').value,
            this.registerForm.get('nom').value,
            this.registerForm.get('prenom').value,
            this.registerForm.get('password').value,

        ]

        //on apelle la la fonction qui va faire le post
        console.log(this.registerForm.get('mail').value)

        this._registerservice.addUser(user)

        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Votre compte a été crée, vous pouvez vous identifier', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}


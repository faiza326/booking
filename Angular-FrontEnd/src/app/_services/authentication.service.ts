﻿//Le service d'authentification est utilisé pour se connecter et se déconnecter de l'application, pour se connecter.
// Il affiche les informations d'identification de l'utilisateur sur l'API et vérifie la réponse pour un jeton JWT.
// S'il en existe un, cela signifie que l'authentification a réussi et que les détails de l'utilisateur, y compris le jeton, sont ajoutés. au stockage local.
// Les détails de l'utilisateur connecté sont stockés dans la mémoire de stockage locale. L'utilisateur reste donc connecté s'il actualise le navigateur et entre les sessions du navigateur jusqu'à sa déconnexion.

// Le service d'authentification présente deux propriétés permettant d'accéder à l'utilisateur actuellement connecté.
// L'observable currentUser peut être utilisé lorsque vous souhaitez qu'un composant se mette à jour de manière réactive lorsqu'un utilisateur se connecte ou se déconnecte.
// La propriété currentUserValue peut être utilisée lorsque vous souhaitez simplement obtenir la valeur actuelle de l'utilisateur connecté

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {User} from '../user'

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(mail, password) {

        const httpHeaders: HttpHeaders = new HttpHeaders({'mail': mail, 'password': password})
        httpHeaders.append('Content-Type','application/json');
        httpHeaders.append('Accept','*/*');

        var data = {
            'mail': 'mail', //user[0].toString(),
            'nom': "jean", //user[1].toString(),
            'prenom': "jean", //user[2].toString(),
            'password': "jean", //user[3].toStrin)g(),
            'tel_perso': "jean" //user[4].toString()
        }


        return this.http.post<any>(`http://localhost:8080/admin/check`, data, { headers: httpHeaders })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}

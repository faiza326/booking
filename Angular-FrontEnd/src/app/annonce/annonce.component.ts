import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Annonce } from '../annonce';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Injectable } from '@angular/core';
import {User} from '../user'
import { UserService, AuthenticationService } from '../_services';
import { first } from 'rxjs/operators';

@Injectable()
@Component({
  selector: 'app-annonce',
  templateUrl: './annonce.component.html',
  styleUrls: ['./annonce.component.css']
})

export class AnnonceComponent implements OnInit {
  currentUser: User;
  users = [];
  annonces: Annonce[];
  closeResult: string;
  editForm: FormGroup;
  deleteID: number;

  constructor(
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
   }

  ngOnInit(): void {
    this.loadAllUsers();
    this.getAnnonces();
    this.editForm=this.fb.group({
      idAnnonce: [''],
      adresse: [''],
      surface: [''],
      nbrePiece: [''],
      equipement: [''],
      description: [''],
      etoile: [''],
      dateDebut: [''],
      dateFin: ['']
    });
  }
  private loadAllUsers() {
       this.userService.getAll()
           .pipe(first())
           .subscribe(users => this.users = users);
  }

  //Fonction pour afficher toutes les annonces
  getAnnonces(){
    this.httpClient.get<any>('http://localhost:8080/annonces').subscribe(
      response => {
        console.log(response);
        this.annonces = response;

      }
    );
  }

  //Affichage de modal-formulaire- pour ajouter une annonce
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  //fonction pour ajouter une annonce
  onSubmit(f: NgForm) {
    console.log(f.value);
    const url = 'http://localhost:8080/admin/annonce/ajouter';
    this.httpClient.post(url, f.value)
      .subscribe((result) => {
        this.ngOnInit(); //reload the table
         console.log("valeurs des champs",f.value);
         console.log("resultat yes bab",result);

      });
    this.modalService.dismissAll(); //fermer le modal
  }

  //Afficher le détail de l'annonce
  openDetails(targetModal: any, annonce: Annonce) {
     this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
     document.getElementById('adresse2').setAttribute('value',annonce.adresse);
     document.getElementById('surface2').setAttribute('value', String(annonce.surface));
     document.getElementById('nbrePiece2').setAttribute('value', String(annonce.nbrePiece));
     document.getElementById('equipement2').setAttribute('value', annonce.equipement);
     document.getElementById('description2').setAttribute('value', annonce.description);
     document.getElementById('etoile2').setAttribute('value', String(annonce.etoile));
     document.getElementById('dateDebut2').setAttribute('value',String( annonce.dateDebut));
     document.getElementById('dateFin2').setAttribute('value',String(annonce.dateFin));
  }

  //Affichage de modal avec toutes les informations de l'annonce a modifier
  openEdit(targetModal: any, annonce: Annonce) {
       this.modalService.open(targetModal, {
        centered: true,
        backdrop: 'static',
        size: 'lg'
      });
       this.editForm.patchValue( {
          idAnnonce: annonce.idAnnonce,
          adresse: annonce.adresse,
          surface: annonce.surface,
          nbrePiece: annonce.nbrePiece,
          equipement: annonce.equipement,
          description: annonce.description,
          etoile: annonce.etoile,
          dateDebut: annonce.dateDebut,
          dateFin: annonce.dateFin
        });
    }

    //Modifier une annonce
    onSave() {
      const editURL = 'http://localhost:8080/admin/annonce/modifier/' + this.editForm.value.idAnnonce ;
      console.log(this.editForm.value);
      this.httpClient.put(editURL, this.editForm.value)
        .subscribe((results) => {
          this.ngOnInit();
          this.modalService.dismissAll();
        });
    }

    openDelete(targetModal: any, annonce: Annonce) {
      this.deleteID = annonce.idAnnonce,
      this.modalService.open(targetModal, {
        backdrop: 'static',
        size: 'lg'
      });
    }

    onDelete() {
      const deleteURL = 'http://localhost:8080/admin/annonce/suprimer/' + this.deleteID;
      this.httpClient.delete(deleteURL)
        .subscribe((results) => {
          this.ngOnInit();
          this.modalService.dismissAll();
        });
    }
     onDeleteAll() {
          this.httpClient.delete<any>('http://localhost:8080/admin/annonces').subscribe((results) => {
                 this.ngOnInit();
                 this.modalService.dismissAll();
                 this.getAnnonces();
          });
     }
}

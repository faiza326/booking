package com.example.annonce.immobiliere;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdminRepository extends CrudRepository<Admin, Integer> {

    @Query("select u from Admin u where u.id_admin = ?1")
    Admin findOneUserWithId(int id);
    List<Admin> findAll();
    @Query("select u from Admin u where u.mail = :mail and u.password = :password")
    Iterable<Admin> checkConnexionUser(@Param("mail") String mail, @Param("password") String password);
}

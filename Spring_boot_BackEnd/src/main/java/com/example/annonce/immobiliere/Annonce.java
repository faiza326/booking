package com.example.annonce.immobiliere;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="annonces")
public class Annonce {

    private int idAnnonce;
    private String adresse;
    private float surface;
    private int nbrePiece;
    private String equipement;
    private String description;
    private int etoile;
    private Date dateDebut;
    private Date dateFin;
    private Admin admin;

    public Annonce(int idAnnonce,String adresse, float surface, int nbrePiece,String equipement, String description, int etoile) {
        this.idAnnonce=idAnnonce;
        this.adresse = adresse;
        this.surface = surface;
        this.nbrePiece = nbrePiece;
        this.description = description;
        this.etoile = etoile;
        this.equipement=equipement;
    }

    public Annonce() {
    }

    public Annonce(int idAnnonce, String adresse, float surface, int nbrePiece,String equipement, String description, int etoile, Date dateDebut, Date dateFin, Admin admin)
    {
        this.idAnnonce = idAnnonce;
        this.adresse = adresse;
        this.surface = surface;
        this.nbrePiece = nbrePiece;
        this.equipement=equipement;
        this.description = description;
        this.etoile = etoile;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.admin = admin;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getIdAnnonce() {
        return idAnnonce;
    }
    public void setIdAnnonce(int idAnnonce) {
        this.idAnnonce = idAnnonce;
    }

    @ManyToOne
    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String getEquipement() {
        return equipement;
    }

    public void setEquipement(String equipement) {
        this.equipement = equipement;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getEtoile() {
        return etoile;
    }

    public void setEtoile(int etoile) {
        this.etoile = etoile;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public float getSurface() {
        return surface;
    }

    public void setSurface(float surface) {
        this.surface = surface;
    }

    public int getNbrePiece() {
        return nbrePiece;
    }

    public void setNbrePiece(int nbrePiece) {
        this.nbrePiece = nbrePiece;
    }



}

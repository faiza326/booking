package com.example.annonce.immobiliere;

import org.aspectj.apache.bcel.classfile.Unknown;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:9002")
@RestController
public class AnnonceController {


    @Autowired
    private AnnonceService adminService;

    @GetMapping("annonces")
    public List<Annonce> getAnnonces(){
        return adminService.getAnnonces();
    }

    @GetMapping("annonce/{id}")
    public Annonce getAnnonce(@PathVariable("id")int id){
        return adminService.getAnnonce(id);
    }

    @PostMapping("admin/annonce/ajouter")
    public void addAnnonces(@RequestBody Annonce annonce){
            adminService.addAnnonce(annonce);
    }

    @PutMapping("admin/annonce/modifier/{id}")
    public void updateAnnonce(@PathVariable("id")int id, @RequestBody Annonce annonce){
            adminService.updateAnnonce(id,annonce);
    }

    @DeleteMapping("admin/annonce/suprimer/{id}")
    public void deleteAnnonce(@PathVariable("id")int id){
        adminService.deleteAnnonce(id);
    }

    @DeleteMapping("admin/annonces")
    public void deleteAllAnnonce(){
        adminService.deleteAllAnnonce();
    }

}

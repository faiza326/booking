package com.example.annonce.immobiliere;

import exceptions.IntrouvableException;
import org.aspectj.apache.bcel.classfile.Unknown;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Service
public class AnnonceService {

    @Autowired
    private AnnonceRepository repository;

    private Admin admin;

    public List<Annonce> getAnnonces(){

            return repository.findAll();
    }

    public Annonce getAnnonce(int id){
        Annonce annonceById = repository.findById(id);
        if(annonceById==null) throw new IntrouvableException("L'annonce avec l'id " + id + " est INTROUVABLE.");
        else
            return annonceById;
    }

    public String addAnnonce(Annonce annonce) {
            repository.save(annonce);
            return  "L'annonce avec l'id" + annonce.getIdAnnonce() + "a été enregistré" ;
    }

    public String updateAnnonce(int id,Annonce annonceDetails)  {
        Annonce annonce = repository.findById(id);
        if(annonce==null) throw new IntrouvableException("L'annonce avec l'id " + id + " est INTROUVABLE.");

        else {
            annonce.setAdresse(annonceDetails.getAdresse());
            annonce.setSurface(annonceDetails.getSurface());
            annonce.setNbrePiece(annonceDetails.getNbrePiece());
            annonce.setEquipement(annonceDetails.getEquipement());
            annonce.setDescription(annonceDetails.getDescription());
            annonce.setEtoile(annonceDetails.getEtoile());
            annonce.setDateDebut(annonceDetails.getDateDebut());
            annonce.setDateFin(annonceDetails.getDateFin());
            repository.save(annonce);

            return  "L'annonce avec l'id" + id + "a été modifié" ;
        }
    }

    public String deleteAnnonce(int id)
    {
        Annonce annonceById = repository.findById(id);
        if(annonceById==null) throw new IntrouvableException("L'annonce avec l'id " + id + " n'existe pas.");
        else
             repository.deleteById(id);
            return  "L'annonce avec l'id" + id + "a été supprimée" ;
    }

    public String deleteAllAnnonce(){
        repository.deleteAll();
         return  "Toutes les annonces ont été supprimée" ;
    }
}
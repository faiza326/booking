package com.example.annonce.immobiliere;

import exceptions.IntrouvableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "http://localhost:9002")
@RequestMapping(path="/admin")
public class AdminController {
    @Autowired
    private AdminRepository AdminRepository;

    @PostMapping(path="/ajouter")
    public @ResponseBody String ajoutParticulier(@RequestBody Admin admin) {

        AdminRepository.save(admin);
        return "Saved";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Iterable<Admin> getlistUser() throws Exception{

        return AdminRepository.findAll();

    }
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Admin findOneUserWithSonId(@RequestParam Integer id) throws Exception{
        return AdminRepository.findOneUserWithId(id);
    }

    /*@GetMapping("admin/find/{id}")
    public Admin getAdmin(@RequestParam("id") int id){
        Admin adminById = AdminRepository.findOneUserWithId(id);
        if(adminById==null) throw new IntrouvableException("L'admin avec l'id " + id + " est INTROUVABLE.");
        else
            return adminById;
    }*/

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Iterable<Admin> checkConnexionUser(@RequestHeader(value="mail") String mail,
                                                            @RequestHeader(value="password") String password) throws Exception{
        return AdminRepository.checkConnexionUser(mail, password);
    }

    /*@PostMapping("admin/check")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Admin> checkConnexionUser(@RequestBody String mail,
                                              @RequestBody String password){
        return AdminRepository.checkConnexionUser(mail, password);
    }*/



}

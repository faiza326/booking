package com.example.annonce.immobiliere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnonceImmobiliereApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnnonceImmobiliereApplication.class, args);
	}

}
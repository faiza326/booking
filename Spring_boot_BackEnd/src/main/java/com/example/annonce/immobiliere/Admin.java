package com.example.annonce.immobiliere;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // This tells Hibernate to make a table out of this class
public class Admin {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    protected Integer id_admin;
    protected String mail;
    protected String nom;
    protected String prenom;
    protected String password;
    private List<Annonce> annonces = new ArrayList<Annonce>();

    public List<Annonce> getAnnonces() {
        return annonces;
    }

    public void setAnnonces(List<Annonce> annonces) {
        this.annonces = annonces;
    }

    public Admin(String mail, String nom, String prenom, String password) {
        this.mail = mail;
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
    }
    public Admin() {
    }


    public Integer getId_admin() {
        return id_admin;
    }

    public void setId_admin(Integer id_admin) {
        this.id_admin = id_admin;
    }
    public Integer getIdAdmin() {
        return id_admin;
    }
    public String getMail() {
        return this.mail;
    }
    public String getNom() {
        return this.nom ;
    }
    public String getPrenom() {
        return this.prenom;
    }
    public String getPassword() {
        return this.password;
    }
    public void setIdAdmin(Integer id_admin) {
        this.id_admin = id_admin;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}

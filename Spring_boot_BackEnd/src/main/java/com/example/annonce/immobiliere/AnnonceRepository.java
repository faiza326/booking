package com.example.annonce.immobiliere;

import org.aspectj.apache.bcel.classfile.Unknown;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface AnnonceRepository extends JpaRepository<Annonce, Integer> {
    Annonce findById(int id);
    List<Annonce> findAll();

}
